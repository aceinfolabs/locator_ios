//
//  AboutViewController.swift
//  Locator
//
//  Created by fazal.rahman on 05/12/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    var viewParent : UIViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func btnAboutClick(_ sender: Any) {
        
        Globals.mainNavigationController?.popToViewController( viewParent!, animated:true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
