//
//  AddDetailsViewController.swift
//  
//
//  Created by fazal.rahman on 02/07/18.
//

import UIKit
import Contacts
import MapKit


class AddDetailsViewController: UIViewController {
    
    @IBOutlet weak var viewChooseCategory: UIView!
    @IBOutlet weak var viewLocationDetails: UIView!
    @IBOutlet weak var viewAddContact: UIView!
    @IBOutlet weak var btnSent: UIButton!
    
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var btnChooseCategory: UIButton!
    
    weak var ParentClass:HomeViewController?
    
    var viewParent: UIViewController?
    var categoryname:String?
     var imageData:String?
    var phonenumber: String?
    var contactname :String?
    var selectedContacts = [CNContact]()
    
    // save location
    var address:[String]?
    var country: String?
    var city: String?
    var latitude: CLLocationDegrees?
    var longitude:CLLocationDegrees?
    var name:String?
    var category_id:Int?
    
    var findco_ordinates : String?
    var currentmapco_ordinates : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        
        self.lblCategoryName.text = "Choose Category"
        self.lblContact.text = "Contact"
        
        self.btnSent.layer.cornerRadius = 25
        btnSent.clipsToBounds = true
        
        self.dismissKeyboard()
       
     
        //find on map codinats display on label
        
        if( findco_ordinates != nil ){
        
            self.lblLocation.text = findco_ordinates
            
        }
        // current location coodintes display on label
        if(Globals.currentlocationco_ordinates != nil){
            
            self.lblLocation.text = Globals.currentlocationco_ordinates
    
        }
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if(categoryname != nil  )
        {
            self.lblCategoryName.text = categoryname as String?
        }
            if (imageData != nil) {
            let url = URL(string:  imageData!)
            if(url != nil){
                Common.GetInstance().getDataFromUrl(url: url!) { data, response, error in
                    guard let data = data, error == nil else { return }
                    
                    DispatchQueue.main.async {
                        self.imgCategory.image = UIImage(data: data)
                    }
                }
            }
        }
    
        if (contactname != nil)
        {
            self.lblContact.text = self.contactname as String?
        }
        
    }
   
    @IBAction func btnChooseCtegoryClick(_ sender: Any) {
        
         UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.btnChooseCategory.isEnabled = false
        Common.GetInstance().ListCategories(completionHandler: getServerResponse)
        
    }
    
    func getServerResponse(ErrorCode : Int, dataReceived: NSDictionary?, ErrorMessage : String) -> Void {
        
        DispatchQueue.main.async {
            self.btnChooseCategory.isEnabled = true
            UIApplication.shared.isNetworkActivityIndicatorVisible = false

            if(ErrorCode == 0){

                    let dicLocators = dataReceived
                Globals.categoryDetails = dicLocators!["data"] as? NSArray

                let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
                newViewController.newViewController = self
                newViewController.categorytitle = self.categoryname
                Globals.mainNavigationController?.pushViewController(newViewController, animated: true)

                }
            }
        }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)

    }
    @IBAction func BtnSentClick(_ sender: Any) {
        
        if(txtDescription.text?.replacingOccurrences(of: " ", with: "") == ""){
            
            Common.GetInstance().showAlert(viewController: self, title: "Add Titite", message: "Please Enter Title.", completion: {(action:UIAlertAction)->Void in
            })
            
            
        }
        else  if(lblContact.text?.replacingOccurrences(of: " ", with: "") == ""){
            
            Common.GetInstance().showAlert(viewController: self, title: "Contact", message: "Please Choose Contact.", completion: {(action:UIAlertAction)->Void in
            })
        }
        
       // Common.GetInstance().SentLocation(message:findco_ordinates, completionHandler: getServerResponseSent)
    }
         
    func getServerResponseSent(ErrorCode : Int, dataReceived: NSDictionary?, ErrorMessage : String) -> Void {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if(ErrorCode == 0){
                
                let dicLocators = dataReceived
                Globals.categoryDetails = dicLocators!["data"] as? NSArray
                
                
            let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                
                    let iisssent = true
                
                newViewController.issent = iisssent
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
                
                
            }
        }
        }
    
    @IBAction func btnChooseLocationClick(_ sender: Any) {
        
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "FindOnMapViewController") as! FindOnMapViewController
        
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
        
    }
    
    @IBAction func btnChooseContactCLICK(_ sender: Any) {
        
        
      //  Globals.mainNavigationController?.popViewController(animated: true)
        
   let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController

        newViewController.parentVc = self
        newViewController.selectedContacts = selectedContacts
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
