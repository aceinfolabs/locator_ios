//
//  AppDelegate.swift
//  Locator
//
//  Created by fazal.rahman on 31/05/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import FirebaseMessaging
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MKMapViewDelegate,CLLocationManagerDelegate {

    var window: UIWindow?
    
    let gcmMessageIDKey = "gcm.message_id"
    
    
    var locationManager = CLLocationManager()
    var myViewController: currentMapViewController!
    var findViewController: FindOnMapViewController!


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
//        GMSServices.provideAPIKey("AIzaSyAEbU1QQAnaAHVIWOGd91oTgfZ_VEveKJs")
//        GMSPlacesClient.provideAPIKey("AIzaSyAEbU1QQAnaAHVIWOGd91oTgfZ_VEveKJs")
        
        //3083FB
        //2875A0 safe AREA view
        //208CE1 view color
        
        //check whether user data is available
        //if yes implement the below code
       /* let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        let nav = storyBoard.instantiateInitialViewController() as? UINavigationController
        let homeVC = storyBoard.instantiateViewController(withIdentifier: "HomeViewController")
        nav?.setViewControllers([homeVC], animated: true)
        self.window?.rootViewController = nav
        */
        
        Globals.mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        Globals.mainNavigationController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "NavigationController") as? UINavigationController
        var initialViewController:UIViewController?
        initialViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.window?.rootViewController = Globals.mainNavigationController
        Globals.mainNavigationController?.setViewControllers([initialViewController!], animated: true)

        //firebase
        
         FirebaseApp.configure()
         Messaging.messaging().delegate = self
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        
        //current location
    
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
    
        return true
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if Globals.LocationCoordinates == nil {
            
            let location: CLLocation = locations.last!
            print("Location: \(location)")
            
            
            Globals.LocationCoordinates = location.coordinate
            
            let cood = Globals.LocationCoordinates!
            let latitude = String?(cood.latitude.description)
            let longitude = String?(cood.longitude.description)
            
            Globals.currentlocationco_ordinates = latitude! + "," + longitude!
            
        }
        locationManager.stopUpdatingLocation()
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       print("Error")
    }

}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        
        Globals.FCMToken = fcmToken
        
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
// TODO: If necessary send token to application server.
// Note: This callback is fired at each app startup and whenever a new token is generated.
        
        func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
            print("Received data message: \(remoteMessage.appData)")
        }
        
    }
    
}

