//
//  CategoryViewController.swift
//  Locator
//
//  Created by fazal.rahman on 16/07/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
   
    @IBOutlet weak var tblCatogery: UITableView!
    
    var arrCategoryData:NSArray?
    var categorytitle : String?
   
    weak var newViewController: AddDetailsViewController?
    weak var newViewControllerReceive: ReceivedAddDetailsViewController?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.arrCategoryData = Globals.categoryDetails!
        self.tblCatogery.reloadData()
        
    }
    @IBAction func btnBackClick(_ sender: Any) {
        Globals.mainNavigationController?.popViewController(animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

           return (Globals.categoryDetails?.count)!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cellData:NSDictionary =  self.arrCategoryData![indexPath.row] as! NSDictionary
        
        if((newViewController) != nil){
        
        newViewController?.categoryname = cellData["name"] as? String
        
        let imagePath = cellData["image"] as? String
      
        newViewController?.imageData = imagePath
        
        Globals.mainNavigationController?.popViewController(animated: true)
        }
        else{
            
            newViewControllerReceive?.categoryname = cellData["name"] as? String
            
            newViewControllerReceive?.category_id = cellData["id"] as? Int
            
            let imagePath = cellData["image"] as? String
            
            newViewController?.imageData = imagePath
            
            Globals.mainNavigationController?.popViewController(animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellData:NSDictionary =  self.arrCategoryData![indexPath.row] as! NSDictionary
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        
        cell.lbtCategoryName?.text = cellData["name"] as? String
        
        
        let imagePath = cellData["image"] as? String
        let url = URL(string:  imagePath!)
        
        if(url != nil && self.arrCategoryData!.count > 0){
            Common.GetInstance().getDataFromUrl(url: url!) { data, response, error in
                guard let data = data, error == nil else { return }

                DispatchQueue.main.async {
                    cell.imgCategory.image =  UIImage(data: data)
                }
            }
        }
        return cell
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
