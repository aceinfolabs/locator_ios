//
//  ChooseLocatorsViewController.swift
//  Locator
//
//  Created by fazal.rahman on 14/08/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class ChooseLocatorsViewController:UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet weak var tblLocators: UITableView!
    
    @IBOutlet weak var searchBarLocators: UISearchBar!
    
    var viewParent: UIViewController?
    
    var filteredLocations : [NSDictionary]?
    var isSearching = false
    var searchAactive: Bool?
    var   loo : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.searchBarLocators.delegate = self
        self.tblLocators.reloadData()
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
      //  if let  _ = self.viewParent {
            
           // let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
           // Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
       // }
        
       // else{
       // Globals.mainNavigationController?.popViewController(animated: true)
            
        //}
        
        Globals.mainNavigationController?.popViewController(animated: true)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching{
            return filteredLocations!.count
        }
        
        return (Globals.userLocationData?.count)!
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "AddDetailsViewController") as! AddDetailsViewController
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocatorsTableViewCell", for: indexPath) as! LocatorsTableViewCell
        
        if isSearching{
            
        let cellDataSearch:NSDictionary = self.filteredLocations![indexPath.row]
        
        cell.lblTitle.text = cellDataSearch["short_description"] as? String
        let city =  cellDataSearch["city"] as? String
        let country =  cellDataSearch["country"] as? String
        let zip =  cellDataSearch["zip"] as? String
        cell.lblAdress.text = city! + "," + country! + "," + zip!
        
        let imagePath = cellDataSearch["image"] as? String
        
        let url = URL(string:  imagePath!)
        if(url != nil && Globals.userLocationData!.count > 0){
            
            Common.GetInstance().getDataFromUrl(url: url!) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async {
                    cell.imgLocator.image =  UIImage(data: data)
                }
            }
        }
        else{
            cell.imgLocator.backgroundColor = UIColor.gray
        }
        }
        else{
        let cellData:NSDictionary = Globals.userLocationData![indexPath.row] as! NSDictionary
        cell.lblTitle.text = cellData["short_description"] as? String
        let city =  cellData["city"] as? String
        let country =  cellData["country"] as? String
        let zip =  cellData["zip"] as? String
        cell.lblAdress.text = city! + "," + country! + "," + zip!
        
        let imagePath = cellData["image"] as? String
        
        
        let url = URL(string:  imagePath!)
        if(url != nil && Globals.userLocationData!.count > 0){
            
            Common.GetInstance().getDataFromUrl(url: url!) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async {
                    cell.imgLocator.image =  UIImage(data: data)
                }
            }
        }
        else{
            cell.imgLocator.backgroundColor = UIColor.gray
        }
        }
        
        return cell
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchBarLocators.text == nil || searchBarLocators.text == ""{
            
         isSearching = false
            
            view.endEditing(true)
            
            self.tblLocators.reloadData()
        }
        else{
            
          isSearching = true
            
            let locationsArray = Globals.userLocationData as? [NSDictionary]
            
            self.filteredLocations = locationsArray?.filter( {(($0["short_description"] as? String)?.lowercased().contains(searchText.lowercased()))! })
            
                self.tblLocators.reloadData()
    
        
    }
    
    }
   
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
