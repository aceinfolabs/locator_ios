//
//  Common.swift
//  Locator
//
//  Created by fazal.rahman on 19/06/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import Foundation
import MapKit



class Common {
    
    static var instance:Common?
    enum ServerCallType: String {
        case get = "GET"
        case post = "POST"
        case patch = "PATCH"
    }
    
    static func GetInstance()->Common!{
        if(instance == nil){
            instance = Common.init()
        }
        return instance!
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
    
    func showAlert(viewController:UIViewController, title:String, message:String, completion:((UIAlertAction)->Void)?){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let retryAction = UIAlertAction.init(title: "Ok", style: .default, handler: completion)
        alertController.addAction(retryAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    func doServerCall(urlString:String, callType:ServerCallType, postData:Data?,withToken:Bool?, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Swift.Void){
        
        let url = URL(string: urlString)
        
        var request = URLRequest(url: url!)
        
        if(withToken == true){
            
            request.setValue("Bearer" + " " + Globals.authToken!, forHTTPHeaderField: "Authorization")
            
            if(postData != nil){
                request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            }
        }
            
        else{
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        }
        request.httpMethod = callType.rawValue
        request.httpBody = postData
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: completionHandler)
        
        task.resume()
        
    }
    
    func LoginUser(Username:String, Password:String, completionHandler: @escaping (Int, NSDictionary?, String) -> Swift.Void){
        
        var stringToPost = "username=" + Username
        
        stringToPost.append("&password=" + Password)
        
        stringToPost.append("&client_id=" + Globals.client_id)
        
        stringToPost.append("&client_secret=" + Globals.client_secret)
        
        stringToPost.append("&grant_type=" + Globals.grant_type)
        
        let dataToPost = stringToPost.data(using: .utf8)
        
        doServerCall(urlString: Globals.serverBaseURL + "oauth/access_token", callType: .post, postData:dataToPost, withToken: nil){ data, response, error in
            
            
            if let urlResponse = response as? HTTPURLResponse {
                
                switch urlResponse.statusCode {
                case 200...202:
                    
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            let dicResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            
                            completionHandler(0, dicResponse, "")
                            
                        }
                        catch {
                            completionHandler(3, nil, "")
                        }
                    }
                    
                default:
                    
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            let dicResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            
                            completionHandler(1, dicResponse, "")
                            
                        }
                        catch {
                            completionHandler(2, nil, "Unable to login. Please try again later")
                        }
                    }
                }
            }
            
            completionHandler(2, nil, "")
        }
    }
    func downloadProfileImage(id:Int?, completionHandler: @escaping (Int, NSDictionary?, String) -> Swift.Void){
        
        let user_id = String(id!)
        
        doServerCall(urlString: Globals.serverBaseURL + "uploads/" + user_id, callType: .get, postData:nil, withToken: true){ data, response, error in
            if let urlResponse = response as? HTTPURLResponse {
                
                switch urlResponse.statusCode {
                case 200...202:
                    
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            let dicResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            
                            completionHandler(0, dicResponse, "")
                            
                        }
                        catch {
                            completionHandler(3, nil, "")
                        }
                    }
                    
                default:
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            let dicResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            
                            completionHandler(1, dicResponse, "")
                            
                        }
                        catch {
                            completionHandler(2, nil, "Unable to load. Please try again later")
                        }
                    }
                }
            }
            completionHandler(2, nil, "")
        }
    }
    
    func UpdateProfile(id:Int?,first_name:String?, completionHandler: @escaping (Int, NSDictionary?, String) -> Swift.Void){
        
        let user_id = String(id!)
        let stringToPost = "first_name=" + first_name!
        let dataToPost = stringToPost.data(using: .utf8)
        
        doServerCall(urlString: Globals.serverBaseURL + "users/" + user_id, callType: .patch, postData:dataToPost, withToken: true){ data, response, error in
            guard let data = data, error == nil else {
                
                completionHandler(1, nil, String(describing : error))
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                
                completionHandler(httpStatus.statusCode, nil, String(describing : response))
                
            }
            let responseString = String(data: data, encoding: .utf8)
            
            Globals.updateStatus = responseString
            
            // Globals.updateFirstName?.setValue(first_name, forKey: "first_name")
            
            //            if let data = responseString?.data(using: String.Encoding.utf8) {
            //
            //            }
            
            //            else{
            //                completionHandler(2, nil, "The user credentials were incorrect")
            //            }
            completionHandler(2, nil, "")
            
            
        }
    }
    
    func UserLocations(id :Int?, completionHandler: @escaping (Int, Any?, String) -> Swift.Void){
        let user_id = String(id!)
        
        doServerCall(urlString: Globals.serverBaseURL + "users/" + user_id + "/locations", callType: .get, postData:nil, withToken: true){ data, response, error in
            
            if let urlResponse = response as? HTTPURLResponse {
                
                switch urlResponse.statusCode {
                case 200...202:
                    
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            //if(data.count > 0){
                            let diResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            completionHandler(0, diResponse, "")
                            // }
                            
                        }
                        catch {
                            completionHandler(2, nil, "")
                        }
                    }
                    
                default:
                    
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            completionHandler(0, nil, "")
                            
                            let dicResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            
                            completionHandler(1, dicResponse, "")
                            
                        }
                        catch {
                            completionHandler(2, nil, "Unable to login. Please try again later")
                        }
                    }
                }
            }
            
            completionHandler(2, nil, "Unable to connect. Please try again later")
        }
        
    }
    
    func ListCategories(completionHandler: @escaping (Int, NSDictionary?, String) -> Swift.Void){
        
        doServerCall(urlString: Globals.serverBaseURL + "categories", callType: .get, postData:nil, withToken: true){ data, response, error in
            if let urlResponse = response as? HTTPURLResponse {
                
                switch urlResponse.statusCode {
                case 200...202:
                    
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            let dicResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            
                            completionHandler(0, dicResponse, "")
                            
                        }
                        catch {
                            completionHandler(3, nil, "")
                        }
                    }
                    
                default:
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            let dicResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            
                            completionHandler(1, dicResponse, "")
                            
                        }
                        catch {
                            completionHandler(2, nil, "Unable to connect. Please try again later")
                        }
                    }
                }
            }
            completionHandler(2, nil, "")
        }
    }
    
    func CreateGroup(parent_id:Int?,title:String?, completionHandler: @escaping (Int, NSDictionary?, String) -> Swift.Void){
        
        let user_id = String(parent_id!)
        
        var stringToPost = "first_name=" + title!
        
        stringToPost.append("&description=" + Globals.description)
        
        
        let dataToPost = stringToPost.data(using: .utf8)
        
        doServerCall(urlString: Globals.serverBaseURL + "groups/" + user_id, callType: .post, postData:dataToPost, withToken: true){ data, response, error in
            if let urlResponse = response as? HTTPURLResponse {
                
                switch urlResponse.statusCode {
                case 200...202:
                    
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            let dicResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            
                            completionHandler(0, dicResponse, "")
                            
                        }
                        catch {
                            completionHandler(3, nil, "")
                        }
                    }
                    
                default:
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            let dicResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            
                            completionHandler(1, dicResponse, "")
                            
                        }
                        catch {
                            completionHandler(2, nil, "Unable to create group. Please try again later")
                        }
                    }
                }
            }
            completionHandler(2, nil, "")
        }
    }
   
    func UpdateFCMToken(id:Int?,gcm_reg_id:String?, completionHandler: @escaping (Int, NSDictionary?, String) -> Swift.Void){
        
        let user_id = String(id!)
        
        let stringToPost = "gcm_reg_id=" + gcm_reg_id!
        let dataToPost = stringToPost.data(using: .utf8)
        
        doServerCall(urlString: Globals.serverBaseURL + "users/" + user_id, callType: .patch, postData:dataToPost, withToken: true){ data, response, error in
            guard let data = data, error == nil else {
                
                completionHandler(1, nil, String(describing : error))
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                
                completionHandler(httpStatus.statusCode, nil, String(describing : response))
                
            }
            let responseString = String(data: data, encoding: .utf8)
            
            Globals.updateStatus = responseString
            
            completionHandler(2, nil, "")
            
            
        }
    }
    
    func SentLocation(message:String?, completionHandler: @escaping (Int, NSDictionary?, String) -> Swift.Void){
        
        
        var stringToPost = "message=" + message!
        
        stringToPost.append("&mobile_numbers[0]=" + Globals.mobile0)
        
        stringToPost.append("&mobile_numbers[1]=" + Globals.mobile1)
        
        let dataToPost = stringToPost.data(using: .utf8)
        
        
        doServerCall(urlString: Globals.serverBaseURL + "push/messages" , callType: .post, postData:dataToPost, withToken: true){ data, response, error in
            if let urlResponse = response as? HTTPURLResponse {
                
                switch urlResponse.statusCode {
                case 200...202:
                    
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            let dicResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            
                            completionHandler(0, dicResponse, "")
                            
                        }
                        catch {
                            completionHandler(3, nil, "")
                        }
                    }
                default:
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        do {
                            
                            let dicResponse = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            
                            completionHandler(1, dicResponse, "")
                            
                        }
                        catch {
                            completionHandler(2, nil, "Unable to sent location. Please try again later")
                        }
                    }
                }
            }
            completionHandler(2, nil, "")
        }
    }
    
    
    
    func SaveLocation(address:String?,city:String?,country:String?,latitude:CLLocationDegrees?,longitude:CLLocationDegrees?,name:String?,description:String?,category_id:Int?,user_id:Int?,favorite:Int?, completionHandler: @escaping (String) -> Swift.Void){
        
        doServerCall(urlString: Globals.serverBaseURL + "locations", callType: .post, postData:nil, withToken: true){ data, response, error in
            if let urlResponse = response as? HTTPURLResponse {
                
                switch urlResponse.statusCode {
                case 200...202:
                    
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        
                        print(data)
                        
                        completionHandler("Registration Success")
                        
                    }
                    
                default:
                    let responseString = String(data: data!, encoding: .utf8)
                    if let data = responseString?.data(using: String.Encoding.utf8) {
                        
                        print(data)
                        completionHandler("Registration  not Success")
                    }
                }
            }
            completionHandler("status")
        }
    }
    
    func find(){
        
        // Set up the URL request
        let todoEndpoint: String = "192.168.1.85:8080"
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        print("error trying to convert data to JSON")
                        return
                }
                // now we have the todo
                // let's just print it to prove we can access it
                print("The todo is: " + todo.description)
                
                // the todo object is a dictionary
                // so we just access the title using the "title" key
                // so check for a title and print it if we have one
                guard let todoTitle = todo["title"] as? String else {
                    print("Could not get todo title from JSON")
                    return
                }
                print("The title is: " + todoTitle)
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
    }

}
