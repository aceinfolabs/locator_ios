//
//  ContactViewController.swift
//  Locator
//
//  Created by fazal.rahman on 03/07/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//
import UIKit
import Contacts
import AddressBook
import ContactsUI

class ContactViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UISearchBarDelegate {
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var tblContacts: UITableView!
    @IBOutlet weak var searchBarContact: UISearchBar!
    
    weak var parentVc: AddDetailsViewController?
    
    var viewParent: UIViewController?
    var arrcontact = [CNContact]()
    var contactSectionTitles = [String]()

    var phone :String?
    var filteredData = [CNContact]()
    var isSearching = false
    
    
    var selectedContacts = [CNContact]()
    var sections:[(index:Int,length:Int,title:String,contacts: [CNContact], selections: [Bool])] = Array()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchContacts()
        self.btnNext.layer.cornerRadius = 25
        btnNext.clipsToBounds = true
        
        self.searchBarContact.delegate = self
        searchBarContact.returnKeyType = UIReturnKeyType.done
        
    }
    
    func  fetchContacts(){
        let contactStore = CNContactStore()
        let keysToFetch = [
            CNContactFormatter.descriptorForRequiredKeys(for: CNContactFormatterStyle.fullName),
            CNContactEmailAddressesKey,
            CNContactPhoneNumbersKey,
            CNContactImageDataAvailableKey,
            CNContactThumbnailImageDataKey] as [Any]
        
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            print("Error fetching containers")
        }
        var results: [CNContact] = []
        
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
                results.append(contentsOf: containerResults)
                
                self.arrcontact = containerResults.sorted(by: { $0.givenName.localizedLowercase < $1.givenName.localizedLowercase})
                
                // sortSection()
                
                //           self.arrcontact.append(repeatElement(false, count: self.arrcontact.count))
                DispatchQueue.main.async {
                    self.tblContacts.reloadData()
                }
                
            } catch {
                print("Error fetching results for container")
            }
        }
        
        return
    }
    //    func sortSection(){
    //
    //        var index = 0;
    ////        print("contacts count: \(self.arrcontact.count)")
    //        var tempContactArray = [CNContact]()
    //        var tempSelectionArray = [Bool]()
    //        var sectionAddingStarted = false
    //        for (i, item) in self.arrcontact.enumerated() {
    //            let commonprefix = item.givenName.commonPrefix(with: self.arrcontact[index].givenName, options: .caseInsensitive)
    //            if sectionAddingStarted {
    //                tempContactArray.append(item)
    //                tempSelectionArray.append(false)
    //            }
    //
    //            if commonprefix.count == 0 {
    //                sectionAddingStarted = true
    ////                print("Common prefix found at index: \(i)")
    //                let nameString = self.arrcontact[index].givenName.uppercased()
    //
    //                var firstCharacter: Character?
    //                if nameString.count > 0 {
    //                    firstCharacter = nameString[nameString.startIndex]
    //                }
    //                else {
    //                    firstCharacter = " "
    //                }
    //                let title = "\(firstCharacter!)"
    //                let newSection = (index: index, length: i - index, title: title, contacts: tempContactArray, selections: tempSelectionArray)
    //                sections.append(newSection)
    //                tempContactArray.removeAll()
    //                tempSelectionArray.removeAll()
    //                index = i
    //            }
    //        }
    //
    //    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//
//       return sections.count
//    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return sections[section].title
//   }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching{
            return filteredData.count
        }
       
        return (self.arrcontact.count)

       // return sections[section].length
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        self.tblContacts.delegate?.tableView!(tableView, didSelectRowAt: indexPath)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.cellForRow(at: indexPath)?.accessoryType = UITableViewCellAccessoryType.none
        
        let cell = tableView.cellForRow(at: indexPath)
       cell?.contentView.backgroundColor = UIColor.blue
//        isSelectedArray[indexPath.row] = true
        
        
        if isSearching {
            if  selectedContacts.contains(filteredData[indexPath.row]) {
                
                 selectedContacts = selectedContacts.filter ({$0.identifier != filteredData[indexPath.row].identifier})
                cell?.contentView.backgroundColor = .white
                
            }
            else{
                selectedContacts.append(filteredData[indexPath.row])
                cell?.contentView.backgroundColor = .blue
            }
        }
        else {
            if arrcontact.count > 0 && selectedContacts.contains(arrcontact[indexPath.row]) {
            selectedContacts = selectedContacts.filter ({$0.identifier != arrcontact[indexPath.row].identifier})
            cell?.contentView.backgroundColor = .white
            }
                else{
                    
                    selectedContacts.append(arrcontact[indexPath.row])
                    cell?.contentView.backgroundColor = .blue
                }
        }
        
//        sections[indexPath.section].selections[indexPath.row] = true
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactViewCell", for: indexPath) as! ContactViewCell
        
//        let contactArray = sections[indexPath.section].contacts
//        let selectionsArray = sections[indexPath.section].selections
       // let contact = contactArray[indexPath.row]
       // cell.isSelected = selectionsArray[indexPath.row]
       // cell.lblContactName?.text = contact.givenName
        //if selectionsArray[indexPath.row] == true{
            //cell.contentView.backgroundColor = UIColor.blue
      //  }
        
        if isSearching{
          let text = self.filteredData[indexPath.row]
            
            cell.lblContactName.text = text.givenName
           // cell.isSelected = selectedContacts.contains(filteredData[indexPath.row])
            
            if selectedContacts.contains(filteredData[indexPath.row]) {
                cell.contentView.backgroundColor = UIColor.blue
            }
            
            if text.imageDataAvailable {
                cell.imgContactImage.image = UIImage.init(data:text.thumbnailImageData!)
                
            }
        }
        else {
            let contact = self.arrcontact[indexPath.row]
            
            
            cell.lblContactName.text = contact.givenName
            
           // cell.isSelected = selectedContacts.contains(arrcontact[indexPath.row])
            
            if selectedContacts.contains(arrcontact[indexPath.row]) {
                print("contact Selected: \(contact.givenName)")
                cell.contentView.backgroundColor = .blue
            }
            if contact.imageDataAvailable {
                cell.imgContactImage.image = UIImage.init(data:contact.thumbnailImageData!)
            }
        }
        
//        let phone = contact.phoneNumbers
//        print(phone[0].value.stringValue)
//        let label = UILabel()
//        label.text = contact.phoneNumbers[indexPath.row].value.stringValue
        return cell
    }
    
//    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
//        return sections.map { $0.title }
//    }
//    
//    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
//        return index
//    }
//
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        let contact = self.arrcontact[indexPath.row]
//        if selectedContacts.contains(arrcontact[indexPath.row]) {
//            print("contact Selected: \(contact.givenName)")
//
//            cell.contentView.backgroundColor = .blue
//            tableView.selectRow(at: indexPath, animated: false, scrollPosition: UITableViewScrollPosition.none)
//        }
//
//    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if searchBarContact.text == nil || searchBarContact.text == ""{

            isSearching = false

            view.endEditing(true)

           self.tblContacts.reloadData()
        }
        else{

             isSearching = true
            
            filteredData = self.arrcontact.filter({$0.givenName.lowercased().contains(searchText.lowercased())})
        
          self.tblContacts.reloadData()
        }

    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }

    @IBAction func btnBackClick(_ sender: Any) {
        
      Globals.mainNavigationController?.popToViewController(parentVc!, animated: true)
        
    }
    
    @IBAction func btnAddClick(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "New Contact ", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            let  newviewcontroller = Globals.mainStoryboard?.instantiateViewController(withIdentifier:"CreateNewContactViewController") as! CreateNewContactViewController
            Globals.mainNavigationController?.pushViewController(newviewcontroller, animated: true)
            
        })
        
        let deleteAction = UIAlertAction(title: "New Group", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            let  newviewcontroller = Globals.mainStoryboard?.instantiateViewController(withIdentifier:"CreateNewGroupViewController") as! CreateNewGroupViewController
            Globals.mainNavigationController?.pushViewController(newviewcontroller, animated: true)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(deleteAction)
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
       
    }
    
    @IBAction func btnNextClick(_ sender: Any) {
        
        let givenNames = selectedContacts.compactMap{ $0.givenName }.joined(separator: ",")
        parentVc?.contactname = givenNames
        let selectedPhoneNumbers = selectedContacts.compactMap{ $0.phoneNumbers[0].value.stringValue}.joined(separator: ",")
        parentVc?.phonenumber = selectedPhoneNumbers
        parentVc?.selectedContacts = selectedContacts
        
        Globals.mainNavigationController?.popViewController(animated: true)
        
    }

}
