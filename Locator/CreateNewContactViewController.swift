//
//  CreateNewContactViewController.swift
//  Locator
//
//  Created by fazal.rahman on 31/07/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import Contacts
import ContactsUI

class CreateNewContactViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var viewaddimage: UIView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var scrollMain: UIScrollView!
    
    @IBOutlet weak var btnsave: UIButton!
    
    @IBOutlet weak var viewButtonSave: UIView!
    let contact = CNMutableContact()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dismissKeyboard()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(_:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(_:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.bounds.height / 2
        self.imgProfilePic.clipsToBounds = true
        
        self.btnsave.layer.cornerRadius = 25
        btnsave.clipsToBounds = true
    }
    
    @objc func keyboardWillAppear(_ notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let inset = keyboardFrame.height // if scrollView is not aligned to bottom of screen, subtract offset
            scrollMain.contentInset.bottom = inset
            scrollMain.scrollIndicatorInsets.bottom = inset
        }
    }
    @objc func keyboardWillDisappear(_ notification: NSNotification) {
        scrollMain.contentInset.bottom = 0
        scrollMain.scrollIndicatorInsets.bottom = 0
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func btnSelectProfileImageClick(_ sender: Any) {
        listEditImageButtonClick()
    }
    
    @IBAction func btnBackClicks(_ sender: Any) {
        Globals.mainNavigationController?.popViewController(animated: true)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            
            imgProfilePic.image = image
            Globals.profileImage = imgProfilePic.image
        }
        else{
            //ERROR MESSAGE
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSaveClick(_ sender: Any) {
        
         if(txtFirstName.text?.replacingOccurrences(of: " ", with: "") == ""){
            
               Common.GetInstance().showAlert(viewController:self, title: "Missing Field", message: "Please enter firstname to continue.", completion: nil)
        }
         else if(txtPhoneNumber.text?.replacingOccurrences(of: " ", with: "") == ""){
            
            Common.GetInstance().showAlert(viewController:self, title: "Missing Field", message: "Please enter phonenumber to continue.", completion: nil)
            
         }
            
         else
         {
        let image = self.imgProfilePic.image
        if let data = UIImagePNGRepresentation(image!)
        {
             contact.imageData = data
        }
      
        let firstname = self.txtFirstName.text!
        contact.givenName = firstname as String
        let lastname = self.txtLastName.text!
        contact.middleName = lastname
        
        contact.phoneNumbers = [CNLabeledValue(
        label:CNLabelPhoneNumberiPhone,
            value:CNPhoneNumber(stringValue:self.txtPhoneNumber.text!))]
        
        let store = CNContactStore()
        let request = CNSaveRequest()
        request.add(contact, toContainerWithIdentifier: nil)
        do{
            try store.execute(request)
        } catch let error{
            print(error)
        }
        Common.GetInstance().showAlert(viewController:self, title: "SAVE CONTACT", message: "save successfully", completion: nil)
        return
        }
        let  newviewcontroller = Globals.mainStoryboard?.instantiateViewController(withIdentifier:"ContactViewController") as! ContactViewController
        Globals.mainNavigationController?.pushViewController(newviewcontroller, animated: true)
        Globals.mainNavigationController?.pushViewController(newviewcontroller, animated: true)
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
