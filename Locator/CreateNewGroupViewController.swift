//
//  CreateNewGroupViewController.swift
//  Locator
//
//  Created by fazal.rahman on 01/08/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class CreateNewGroupViewController: UIViewController {

    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var viewaddProfileimage: UIView!
    
    @IBOutlet weak var txtGroupTitle: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.bounds.height / 2
        self.imgProfilePic.clipsToBounds = true
    }

    @IBAction func btnSelectGroupimageClick(_ sender: Any) {
        listEditImageButtonClick()
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            
            //  self.imgProfilePic
            
            imgProfilePic.image = image
            Globals.profileImage = imgProfilePic.image
        }
        else{
            //ERROR MESSAGE
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnBackClick(_ sender: Any) {
        Globals.mainNavigationController?.popViewController(animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSaveclick(_ sender: Any) {
        
       // let user_id = Globals.loginResponse?["id"] as? Int
//        Common.GetInstance().CreateGroup(parent_id: user_id, title: self.txtGroupTitle.text, completionHandler: (getServerResponsecheckuser))
        
    }
    
    func getServerResponsecheckuser(Errorcode : Int, dataReceived: Any?, errorMessage : String) -> Void {
        DispatchQueue.main.async {
          //  self.btnLogin.isEnabled = true
            if(Errorcode > 0){
                if let response = dataReceived as? [String:Any] {
                    Common.GetInstance().showAlert(viewController: self, title: (response["error"] as? String)!, message: (response["error_description"] as? String)!, completion: nil)
                    return
                }
            }
            else{
               
            
        }
    }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
