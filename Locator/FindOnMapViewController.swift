//
//  FindOnMapViewController.swift
//  Locator
//
//  Created by fazal.rahman on 11/09/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import MapKit


class FindOnMapViewController: UIViewController,MKMapViewDelegate, CLLocationManagerDelegate,UISearchBarDelegate,HandleMapSearch{
    
    func dropPinZoomIn(placemark:MKPlacemark){
        // cache the pin
        //            selectedPin = placemark
        // clear existing pins
        mapView?.removeAnnotations((mapView?.annotations)!)
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        if let city = placemark.locality,
            let state = placemark.administrativeArea {
            annotation.subtitle = "(city) (state)"
        }
        mapView?.addAnnotation(annotation)
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegionMake(placemark.coordinate, span)
        mapView?.setRegion(region, animated: true)
    }
    
    

    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var btnConfirmLocation: UIButton!
    
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
     let annotation = MKPointAnnotation()
    

    var resultSearchController: SearchResultsViewController?
    
   
    var searchResults = [MKLocalSearchCompletion]()
    
    var searchCompleter = MKLocalSearchCompleter()
    
    var savelocation_cooduinates:CLLocationCoordinate2D?
    var co_ordinates: String?
    var viewParent:UIViewController?
    
    var placeName:String?
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     // let  resultSearchController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchResultViewController") as? SearchResultsViewController
        
        self.btnConfirmLocation.layer.cornerRadius = 25
        btnConfirmLocation.clipsToBounds = true
        
        
        appDelegate.locationManager.requestAlwaysAuthorization()
        
        mapView.showsUserLocation = true
       
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
       

        annotation.coordinate = Globals.LocationCoordinates!
        
        annotation.title = "current place"
        mapView.addAnnotation(annotation)
        
        // mapView.delegate = self
    }

    @IBAction func btnSearchClicks(_ sender: Any) {
       
//       resultSearchController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchResultsViewController") as? SearchResultsViewController
//     resultSearchController.viewParent = self
        
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "SearchResultsViewController") as! SearchResultsViewController
        newViewController.viewParent = self
        newViewController.delegate = self
        newViewController.searchResults = searchResults
        self.present(newViewController, animated: true, completion: nil)

    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        Globals.mainNavigationController?.popViewController(animated: true)
        
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { placemarks, error in
            guard let placemark = placemarks?.first, error == nil else {
                completion(nil, error)
                return
            }
            completion(placemark, nil)
        }
    }

    @IBAction func addPin(_ sender: UILongPressGestureRecognizer) {
        
        let locat = sender.location(in: self.mapView)
        
        let coord = self.mapView.convert(locat, toCoordinateFrom: self.mapView)
        
        
        geocode(latitude: coord.latitude, longitude: coord.longitude) { placemark, error in
            
            guard let placemark = placemark, error == nil else { return }
            
            self.placeName  = placemark.name ?? ""
            print("placessss: \(self.placeName!)")
        }
        
         savelocation_cooduinates = coord
        
        let annotation = MKPointAnnotation()
        
        annotation.coordinate = coord
       
        annotation.title = placeName
        
        let lattvalue = String(coord.latitude)
        
        let longvalue = String(coord.longitude)
        
        co_ordinates = longvalue + "," + lattvalue
        
        self.mapView.removeAnnotations(mapView.annotations)
        
        self.mapView.addAnnotation(annotation)
        
    }
    
    @IBAction func btnConfirmLocationClick(_ sender: Any) {
        
//        if co_ordinates == nil{
//            
//            Common.GetInstance().showAlert(viewController: self, title: "Add Titite", message: "Choose a Location.", completion: {(action:UIAlertAction)->Void in
//            })
//        }
        
        if let _ = self.viewParent {
            let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "AddDetailsViewController") as! AddDetailsViewController
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
            
            newViewController.findco_ordinates = co_ordinates
            
        }
        
        else{
          
            let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "ReceivedAddDetailsViewController") as! ReceivedAddDetailsViewController
            
             newViewController.saveFindco_ordinates = co_ordinates
         
              if (savelocation_cooduinates != nil) {
            geocode(latitude: (savelocation_cooduinates?.latitude)!, longitude: (savelocation_cooduinates?.longitude)!) { placemark, error in
                
                guard let placemark = placemark, error == nil else { return }
              
//                if  let add = placemark.addressDictionary!["FormattedAddressLines"] as? [String] {
//                }
                
                let address = "\(placemark.thoroughfare ?? ""), \(placemark.locality ?? ""), \(placemark.subLocality ?? ""), \(placemark.administrativeArea ?? ""), \(placemark.postalCode ?? ""), \(placemark.country ?? "")"
                
                newViewController.address = address
                
                
                newViewController.country = placemark.country
                newViewController.latitude = placemark.location?.coordinate.longitude
                newViewController.longitude = placemark.location?.coordinate.latitude
                newViewController.name = placemark.name ?? ""
                newViewController.city = placemark.subLocality ?? ""
                
            }
        
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
            }
        }
        }

func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
    // handle error
}
}

//extension SearchResultsViewController: HandleMapSearch {
//    func dropPinZoomIn(placemark:MKPlacemark){
//        // cache the pin
//      selectedPin = placemark
//        // clear existing pins
//        mapView?.removeAnnotations((mapView?.annotations)!)
//        let annotation = MKPointAnnotation()
//        annotation.coordinate = placemark.coordinate
//        annotation.title = placemark.name
//        if let city = placemark.locality,
//            let state = placemark.administrativeArea {
//            annotation.subtitle = "(city) (state)"
//        }
//        mapView?.addAnnotation(annotation)
//        let span = MKCoordinateSpanMake(0.05, 0.05)
//        let region = MKCoordinateRegionMake(placemark.coordinate, span)
//        mapView?.setRegion(region, animated: true)
//    }
//}
