//
//  HomeViewController.swift
//  Locator
//
//  Created by fazal.rahman on 31/05/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import MapKit


class HomeViewController: UIViewController,UITableViewDelegate ,UITableViewDataSource,UITabBarDelegate,MenuPopupDelegate{
    
    
    

    @IBOutlet weak var tblPlaces: UITableView!

    @IBOutlet weak var tabBar: UITabBar!
    
    @IBOutlet weak var viewimage: UIView!
    @IBOutlet weak var imgsent: UIImageView!
    
    @IBOutlet weak var viewimagesent: UIView!
    @IBOutlet weak var viewSentLocator: UIView!
    
    @IBOutlet weak var imgSave: UIImageView!
    @IBOutlet weak var viewSaveLocator: UIView!
    
    var viewParent: UIViewController?
    
    var arrMyLocation:NSArray?
    var CategoryImages:[Data?]?
    var dicImages:NSDictionary?
    
    var isrecent = false
    var isfavourite = false
    var filteredLocations : [NSDictionary]?
    var issent = false
    var issave = false
    
    var latitude: String?
    var longitude : String?
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.viewSentLocator.isHidden = true
        self.viewSaveLocator.isHidden = true
        
        
        self.viewSentLocator.isHidden = true
        
        tblPlaces.delegate = self
        tblPlaces.dataSource = self
        self.tabBar.delegate = self
        self.tabBar.unselectedItemTintColor = .white
       // self.tabBar.items![0].imageInsets = UIEdgeInsetsMake(7, 0, -7, 0)
        //self.tabBar.items![1].imageInsets = UIEdgeInsetsMake(7, 0, -7, 0)
        
        
        if(self.issent == true) {
            
            self.viewSentLocator.isHidden = false
            
            self.viewSentLocator.alpha = 0.7
            self.viewSentLocator.backgroundColor = .black
            
            self.viewimage.backgroundColor = .white
            self.viewimage.alpha = 1
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute:{
                self.viewSentLocator.isHidden = true
                
            })
            
        }
        
        if (self.issave == true){
            
            self.viewSaveLocator.isHidden = false
            
            self.viewSaveLocator.alpha = 0.7
            self.viewSaveLocator.backgroundColor = .black
            
            self.viewimagesent.backgroundColor = .white
            self.viewimagesent.alpha = 1
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(4), execute:{
                self.viewSaveLocator.isHidden = true
                
            })
            
        }
        
        
        
        let user_id = Globals.loginResponse?["id"] as? Int
        Common.GetInstance().UserLocations(id: user_id, completionHandler: self.getServerResponselocation)
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {

        
        
//    }
    
    func getServerResponselocation(Errorcode : Int, dataReceived: Any?, errorMessage : String) -> Void {
        DispatchQueue.main.async {
            
            if(Errorcode == 0){
                
                let dicLocators = dataReceived as! NSDictionary
                Globals.userLocationData = dicLocators["data"] as? NSArray
                self.arrMyLocation = Globals.userLocationData
                
                //               self.CategoryImages = [Data?](repeating: nil, count:(self.arrMyLocation?.count)!)
                
            }
        }
        
        DispatchQueue.main.async {
            self.tblPlaces.reloadData()
            
        }
        
    }

//    @IBAction func btnFavoriteClick(_ sender: Any) {
//
//        UIView.animate(withDuration: 0.3) {
//            self.viewRecent.isHidden = true
//            self.viewFavoriteButton.isHidden = false
//
//            self.favoriteButton.setTitleColor(UIColor.init(red: 32.0/255.0, green: 140.0/255.0, blue: 225.0/255.0, alpha: 1), for: UIControlState.normal)
//
//             self.recentButton.setTitleColor(UIColor.gray, for: UIControlState.normal)
//
//            self.tblPlaces.reloadData()
//        }
//
//    }
//
//    @IBAction func btnRecentClick(_ sender: Any) {
//
//        if !isrecent{
//
//        isrecent =  true
//
//        if let locationsArray = self.arrMyLocation as? [NSDictionary] {
//        self.filteredLocations = locationsArray.filter( {$0["show_dash"] as? String == "1"})
//        }
//        }
//
//        UIView.animate(withDuration: 0.3) {
//            self.viewFavoriteButton.isHidden = true
//            self.viewRecent.isHidden = false
//
//            self.recentButton.setTitleColor(UIColor.init(red: 32.0/255.0, green: 140.0/255.0, blue: 225.0/255.0, alpha: 1), for: UIControlState.normal)
//
//            self.favoriteButton.setTitleColor(UIColor.gray, for: UIControlState.normal)
//
//
//            self.tblPlaces.reloadData()
//        }
//    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isrecent{
            return filteredLocations!.count
       }
        
        if (self.arrMyLocation == nil){
            return 0
        }
        else{
            return (self.arrMyLocation?.count)!
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellData:NSDictionary = self.arrMyLocation![indexPath.row] as! NSDictionary
        let latitude: CLLocationDegrees = Double(cellData["latitude"] as! String) ?? 0
        let longitude: CLLocationDegrees = Double(cellData["longitude"] as! String) ?? 0
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Place Name"
        mapItem.openInMaps(launchOptions: options)
        
//        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "NavigateLocationViewController") as! NavigateLocationViewController
//
//
//        newViewController.latitudeString = self.latitude!
//        newViewController.longitudeString = self.longitude
        
        
//        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellData:NSDictionary = self.arrMyLocation![indexPath.row] as! NSDictionary
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeFavoriteCell", for: indexPath) as! HomeFavoriteCell
        
        if isrecent{
            
            cell.lblLocation?.text = cellData["short_description"] as? String
            let city =  cellData["city"] as? String
            let country =  cellData["country"] as? String
            let zip =  cellData["zip"] as? String
            cell.lblAddress.text = city! + "," + country! + "," + zip!
            cell.lblPlace.text =  cellData["name"]  as? String
            
            let imagePath = cellData["image"] as? String ?? ""
            let url = URL(string:  imagePath)
            
            if(url != nil && self.arrMyLocation!.count > 0){
                Common.GetInstance().getDataFromUrl(url: url!) { data, response, error in
                    guard let data = data, error == nil else { return }
                    
                    DispatchQueue.main.async {
                        cell.imglLocation.image =  UIImage(data: data)
                    }
                }
            }
            else{
                cell.imglLocation.backgroundColor = UIColor.gray
            }
            
        }
        
        else{
        cell.lblLocation?.text = cellData["short_description"] as? String
        let city =  cellData["city"] as? String
        let country =  cellData["country"] as? String
        let zip =  cellData["zip"] as? String
        cell.lblAddress.text = city! + "," + country! + "," + zip!
        cell.lblPlace.text =  cellData["name"]  as? String
            
            self.latitude = (cellData["latitude"] as? String)!
            self.longitude = (cellData["longitude"] as? String)!
        
        let imagePath = cellData["image"] as? String
        let url = URL(string:  imagePath!)
        
        if(url != nil && self.arrMyLocation!.count > 0){
            Common.GetInstance().getDataFromUrl(url: url!) { data, response, error in
                guard let data = data, error == nil else { return }
                
                DispatchQueue.main.async {
                    cell.imglLocation.image =  UIImage(data: data)
                }
            }
        }
        else{
            cell.imglLocation.backgroundColor = UIColor.gray
        }
        }
        //add accessory view
        let accessoryView = UIImageView.init(frame: CGRect.init(x: 10, y: 10, width: 20, height: 20))
        accessoryView.image = #imageLiteral(resourceName: "more")    //image gray
        accessoryView.contentMode = .scaleAspectFit
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.didTapAccessoryView))
        accessoryView.isUserInteractionEnabled = true
        accessoryView.addGestureRecognizer(tapGesture)
        cell.accessoryView = accessoryView
        cell.selectionStyle = .none
        return cell
    }
    @objc func didTapAccessoryView() {
        let alert = UIAlertController.init(title: "", message: "", preferredStyle: .actionSheet)
        let edit = UIAlertAction.init(title: "Edit", style: .default) { _ in
            
        }
        let delete = UIAlertAction.init(title: "Delete", style: .default) { _ in
            
        }
        let send = UIAlertAction.init(title: "Send", style: .default) { _ in
            
        }
        let favorite = UIAlertAction.init(title: "Favorite", style: .default) { _ in
            
        }
        
        alert.addAction(edit)
        alert.addAction(delete)
        alert.addAction(send)
        alert.addAction(favorite)
        self.present(alert, animated: true, completion: nil)
    }
    // extension ViewController : UITabBarDelegate {
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item == (self.tabBar.items!)[0] {
    
            if !isrecent{
                
                isrecent =  true
                
                if let locationsArray = self.arrMyLocation as? [NSDictionary] {
                    self.filteredLocations = locationsArray.filter( {$0["show_dash"] as? String == "1"})
                }
            }
                self.tblPlaces.reloadData()
        }
        
        
        
        else if item == (self.tabBar.items!)[1]{
            
          didTapDownwardView()
            
        }
        
        else if item == (self.tabBar.items!)[2]{
    
            didTapUpwardView()
    
        }
        
        else if item == (self.tabBar.items!)[3]{
           
            let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            
            newViewController.viewParent = self
            
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
            
        }
        else if item == (self.tabBar.items!)[4]{
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            
            newViewController.viewParent = self
            
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
    }
    }
    @objc func didTapUpwardView(){
        let alert = UIAlertController.init(title: nil, message: nil, preferredStyle:.actionSheet)
        let current = UIAlertAction.init(title: "Current", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "currentMapViewController") as! currentMapViewController
              newViewController.viewParent = self
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
            
        })
        let saved = UIAlertAction.init(title: "Saved/Recent", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "ChooseLocatorsViewController") as! ChooseLocatorsViewController
           // newViewController.viewParent = self
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
            
        })
        let findOnMap = UIAlertAction.init(title: "Find on Map", style: .default,handler:
        {
            (alert: UIAlertAction!) -> Void in
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "FindOnMapViewController") as! FindOnMapViewController
            newViewController.viewParent = self
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
            
        })
        
        let send = UIAlertAction.init(title: "Send", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "SentListViewController") as! SentListViewController
            newViewController.viewParent = self
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            print("Cancelled")
        })
        
        alert.addAction(current)
        alert.addAction(saved)
        alert.addAction(findOnMap)
        alert.addAction(send)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    @objc func didTapDownwardView(){
        let alert = UIAlertController.init(title: nil, message: nil, preferredStyle: .actionSheet)
      
        let current = UIAlertAction.init(title: "Current",style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "currentMapViewController") as! currentMapViewController
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
            
        })
        let find = UIAlertAction.init(title: "Find on Map", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "FindOnMapViewController") as! FindOnMapViewController
            newViewController.viewParent = self
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
            
        })
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
    {
        (alert: UIAlertAction!) -> Void in
        
        
        print("Cancelled")
    })
        alert.addAction(current)
        alert.addAction(find)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func btnMenuClick(_ sender: Any) {
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: " PopUpViewController") as! PopUpViewController
        newViewController.viewParent = self
        newViewController.menuPopupDelegate = self
        
        newViewController.modalPresentationStyle = .overCurrentContext
        
        Globals.mainNavigationController?.present(newViewController, animated: false)
    }
    @IBAction func currentLocationClick(_ sender: Any) {
//        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
//        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }

}
