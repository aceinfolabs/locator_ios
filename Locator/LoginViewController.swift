//
//  LoginViewController.swift
//  Locator
//
//  Created by fazal.rahman on 31/05/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController {
    
    @IBOutlet weak var textUsername: UITextField!
    @IBOutlet weak var textPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var scrollMain: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(_:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(_:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
        Globals.mainNavigationController?.navigationBar.isHidden = true
        btnLogin.layer.cornerRadius = 25
        btnLogin.clipsToBounds = true
        
        textPassword.isSecureTextEntry = true
        
    }
    
    @objc func keyboardWillAppear(_ notification: NSNotification) {
        if let userInfo = notification.userInfo,
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let inset = keyboardFrame.height // if scrollView is not aligned to bottom of screen, subtract offset
            scrollMain.contentInset.bottom = inset
            scrollMain.scrollIndicatorInsets.bottom = inset
        }
    }
    @objc func keyboardWillDisappear(_ notification: NSNotification) {
        scrollMain.contentInset.bottom = 0
        scrollMain.scrollIndicatorInsets.bottom = 0
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnLoginClick(_ sender: Any) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        btnLogin.isEnabled = false
        
        Common.GetInstance().LoginUser(Username: textUsername.text!, Password: textPassword.text!, completionHandler: getServerResponsecheckuser)
       
    }
    
    func getServerResponsecheckuser(Errorcode : Int, dataReceived: Any?, errorMessage : String) -> Void {
        DispatchQueue.main.async {
            self.btnLogin.isEnabled = true
            if(Errorcode > 0){
                if let response = dataReceived as? [String:Any] {
                    Common.GetInstance().showAlert(viewController: self, title: (response["error"] as? String)!, message: (response["error_description"] as? String)!, completion: nil)
                    return
                }                
            }
            else{
                Globals.usernameSelected = self.textUsername.text!
                Globals.passwordSelected = self.textPassword.text!
                let dicRecieved = dataReceived as! NSDictionary
                Globals.authToken = dicRecieved["access_token"] as? String
                Globals.loginResponse = dicRecieved["data"] as? [String : Any]
                
                let user_id = Globals.loginResponse?["id"] as? Int
                
                Common.GetInstance().downloadProfileImage(id: user_id, completionHandler: self.getServerResponse)
                
               // Common.GetInstance().find()
                
                if Globals.FCMToken != nil {
                
                Common.GetInstance().UpdateFCMToken(id: user_id!, gcm_reg_id: Globals.FCMToken!, completionHandler:  self.getServerResponseFCMToken)
                }
                
                let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
            }
           
        }
    }
    
    func getServerResponseFCMToken(Errorcode : Int, dataReceived: Any?, errorMessage : String) -> Void {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
           
            
            if( Globals.updateStatus! == "Update Success"){
                
            Globals.loginResponse?.updateValue(Globals.FCMToken!, forKey: "gcm_reg_id")
            }
//            Common.GetInstance().showAlert(viewController: self, title: "Update Profile", message: "Profile successfully updated.", completion: {(action:UIAlertAction)->Void in
//
////                Globals.mainNavigationController?.popViewController(animated: true)
//
//            })
        }
    }
    
    func getServerResponse(errorCode : Int, dataReceived: NSDictionary?, errorMessage : String) -> Void {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if(errorCode > 0 ){
              if let response = dataReceived as? [String:Any] {
            Common.GetInstance().showAlert(viewController: self, title: "Error Code: " + "\(errorCode)", message: errorMessage, completion: nil)
                           return
                }
                    }
            else{
                let imageData = dataReceived?["data"] as? NSDictionary
                
                let imagePath = imageData?["url"] as? String ?? ""
                
                let url = URL(string:  imagePath)
                Common.GetInstance().getDataFromUrl(url: url!) { data, response, error in
                    guard let data = data, error == nil else { return }
    
                        let profilePictureSelected = UIImage(data: data)
                        Globals.profileImage = profilePictureSelected!
            }
        }
     }
  }
    
    
    
    
    
    @IBAction func btnForgotPasswordClick(_ sender: Any) {
    }
    @IBAction func btnRegisterClick(_ sender: Any) {
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
