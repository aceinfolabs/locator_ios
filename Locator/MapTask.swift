//
//  MapTask.swift
//  Locator
//
//  Created by fazal.rahman on 09/10/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import GoogleMaps

class MapTask {

    var serverKey = "AIzaSyDLft0nUsc5T4YH-Sp-35NY1BU1iRSklKo"
    //let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    //var lookupAddressResults: Dictionary<String, AnyObject>!
    //var fetchedFormattedAddress: String!
    //    var fetchedAddressLongitude: Double!
    //    var fetchedAddressLatitude: Double!
    
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    var selectedRoute: Dictionary<String, AnyObject>!
    var overviewPolyline: Dictionary<String, AnyObject>!
    var originCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    var originAddress: String!
    var destinationAddress: String!
    
    
    var totalDistanceInMeters: UInt = 0
    var totalDistance: String!
    var totalDurationInSeconds: UInt = 0
    var totalDurationInMinutes: UInt = 0
    var totalDuration: String!
    
    
    init(serverKey: String) {
        self.serverKey = serverKey
    }
    
}


protocol MapRouteProtocol1 {
}

extension MapTask: MapRouteProtocol1 {
    
    enum TravelModes: Int {
        //modes of the map
        case driving
        case walking
        case bicycling
    }
    
   internal func getDirections(_ originLocation: CLLocationCoordinate2D, destinationLocation: CLLocationCoordinate2D, waypoints: Array<String>?, travelMode: TravelModes, completionHandler: @escaping ((_ status: String, _ success: Bool) -> Void)) {

        let origin = "\(originLocation.latitude)" + "," + "\(originLocation.longitude)"
        let destination = "\(destinationLocation.latitude)" + "," + "\(destinationLocation.longitude)"

        var directionsURLString = baseURLDirections + "origin=" + origin + "&destination=" + destination

        // waypoint Initialization
        if let routeWaypoints = waypoints {
            directionsURLString += "&waypoints=optimize:true"
            for waypoint in routeWaypoints {
                directionsURLString += "|" + waypoint
            }
        }

       
        var travelModeString = ""
        switch travelMode.rawValue {
        case TravelModes.walking.rawValue:
            travelModeString = "walking"
        case TravelModes.bicycling.rawValue:
            travelModeString = "bicycling"
        default:
            travelModeString = "driving"
        }

        directionsURLString += "&mode=" + travelModeString
        directionsURLString = directionsURLString + "/\(serverKey)"
        directionsURLString = directionsURLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!

        let directionsURL = URL(string: directionsURLString)

        DispatchQueue.main.async(execute: { () -> Void in
            guard let directionsData: Data = try? Data(contentsOf: directionsURL!) else {return}

            do {
                guard let dictionary: Dictionary<String, AnyObject> = try JSONSerialization.jsonObject(with: directionsData, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: AnyObject] as Dictionary<String, AnyObject>? else {return}
                guard let status = dictionary["status"] as? String else {return}

                if status == "OK"{
                    self.selectedRoute = (dictionary["routes"] as? Array<Dictionary<String, AnyObject>> ?? [])[0]
                    if let overviewPol = self.selectedRoute["overview_polyline"] as? Dictionary<String, AnyObject> {
                        self.overviewPolyline = overviewPol
                    }

                    guard let legs = self.selectedRoute["legs"] as? Array<Dictionary<String, AnyObject>> else {return}
                    let startLocationDictionary = legs[0]["start_location"] as? Dictionary<String, AnyObject> ?? [:]
                    self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as? Double ?? 0, startLocationDictionary["lng"] as? Double ?? 0)
                    let endLocationDictionary = legs[legs.count - 1]["end_location"] as? Dictionary<String, AnyObject> ?? [:]
                    self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as? Double ?? 0, endLocationDictionary["lng"] as? Double ?? 0)

                    self.calculateTotalDistanceAndDuration()
                    completionHandler(status, true)
                    return
                }

            } catch let error as NSError {
                print("json error: \(error.localizedDescription)")
            }
        })
    }
    
    func calculateTotalDistanceAndDuration() {
        let legs = self.selectedRoute["legs"] as? Array<Dictionary<String, AnyObject>> ?? []
        
        totalDistanceInMeters = 0
        totalDurationInSeconds = 0
        
        for leg in legs {
            totalDistanceInMeters += (leg["distance"] as? Dictionary<String, AnyObject> ?? [:])["value"] as? UInt ?? 0
            totalDurationInSeconds += (leg["duration"] as? Dictionary<String, AnyObject> ?? [:])["value"] as? UInt ?? 0
        }
        
        
        let distanceInKilometers: Double = Double(totalDistanceInMeters / 1000)
        totalDistance = "Total Distance: \(distanceInKilometers) Km"
        
        
        let mins = totalDurationInSeconds / 60
        let hours = mins / 60
        let days = hours / 24
        let remainingHours = hours % 24
        let remainingMins = mins % 60
        let remainingSecs = totalDurationInSeconds % 60
        
        totalDuration = "Duration: \(days) d, \(remainingHours) h, \(remainingMins) mins, \(remainingSecs) secs"
    }
    
}


