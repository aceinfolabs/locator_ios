//
//  MapViewController.swift
//  Locator
//
//  Created by fazal.rahman on 06/06/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit


class MapViewController: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate,MenuPopupDelegate {
    //@IBOutlet weak var mapview: UIView!
    
    @IBOutlet weak var mapView: MKMapView!
    var  locationManager = CLLocationManager()

    var viewParent:UIViewController?
    var viewParent1:UIViewController?
    
    var regionHasBeenCentered = false
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.showsUserLocation = true
        mapView.delegate = self
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
    
        if CLLocationManager.locationServicesEnabled() == true {

            if CLLocationManager.authorizationStatus() == .restricted || CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined {

            locationManager.requestWhenInUseAuthorization()
            }
        }

        else{
            print("please chek.....")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations[0]
        
        if !regionHasBeenCentered {
            let span: MKCoordinateSpan = MKCoordinateSpanMake(40.0, 40.0)
            let userLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
            let region: MKCoordinateRegion = MKCoordinateRegionMake(userLocation, span)
            
            mapView.setRegion(region, animated: true)
            regionHasBeenCentered = true
        }
        
        self.mapView.showsUserLocation = true
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        print("unable to access your current Location")
    }

    @IBAction func btnPopUpMenuClick(_ sender: Any) {
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: " PopUpViewController") as! PopUpViewController
        
        newViewController.viewParent = self
        
        newViewController.menuPopupDelegate = self
        
        newViewController.modalPresentationStyle = .overCurrentContext
        
        Globals.mainNavigationController?.present(newViewController, animated: false)
    }
    
    @IBAction func btnMenuListClick(_ sender: Any) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
       Globals.mainNavigationController?.popViewController(animated: true)
        
    }
    
}

