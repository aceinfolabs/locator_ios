//
//  NavigateLocationViewController.swift
//  Locator
//
//  Created by fazal.rahman on 04/10/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class NavigateLocationViewController: UIViewController {
    
    
    @IBOutlet weak var viewNavigation: UIView!

    let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    
    var lookupAddressResults: Dictionary<String, AnyObject>!
    
    var fetchedFormattedAddress: String!
    
    var fetchedAddressLongitude: Double!
    
    var fetchedAddressLatitude: Double!
    
    var MapView: GMSMapView!
    
    var originMarker: GMSMarker!
    
    var destinationMarker: GMSMarker!
    
    var routePolyline: GMSPolyline!
    
    var destinationCoordinate: CLLocation?
    
    let serverKey = "AIzaSyAm0k6TP4RqQ9V8XnA68wZ-E-NnX2xWwYU"

    
    var mapTasks = MapTask(serverKey: "AIzaSyAm0k6TP4RqQ9V8XnA68wZ-E-NnX2xWwYU")
    
    var latitudeString : String?
    var longitudeString: String?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//         let camera = GMSCameraPosition.camera(withTarget: (Globals.LocationCoordinates?.coordinate)!, zoom: 14)
//
//        MapView = GMSMapView.map(withFrame: self.viewNavigation.bounds, camera: camera)
//
//        MapView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//
//        self.viewNavigation.addSubview(MapView)

        let lat = Double(latitudeString!)
        let lon = Double(longitudeString!)

         destinationCoordinate = CLLocation(latitude:lat!, longitude:lon!)
        
        createRoute()

    }
    
    @IBAction func btnMenuClicks(_ sender: Any) {
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: " PopUpViewController") as! PopUpViewController
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func btnCloseClicks(_ sender: Any) {
        
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
    }
    
    
}
    
    extension NavigateLocationViewController: GMSMapViewDelegate {
    
   internal func geocodeAddress(_ address: String!, withCompletionHandler completionHandler: @escaping ((_ status: String, _ success: Bool) -> Void)) {
        if let lookupAddress = address {
            
            var geocodeURLString = baseURLGeocode + "address=" + lookupAddress
            geocodeURLString = geocodeURLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
            
            let geocodeURL = URL(string: geocodeURLString)
            
            DispatchQueue.main.async(execute: { () -> Void in
                let geocodingResultsData = try? Data(contentsOf: geocodeURL!)
                
                do {
                    guard let dictionary: Dictionary<String, AnyObject> = try JSONSerialization.jsonObject(with: geocodingResultsData!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: AnyObject] else {return}
                    
                    guard let status = dictionary["status"] as? String else {return}
                    
                    if status == "OK" {
                        
                        guard let allResults = dictionary["results"] as? Array<Dictionary<String, AnyObject>> else {return}
                        
                        self.lookupAddressResults = allResults[0] as Dictionary<String, AnyObject>
                        
                        // Keep the most important values.
                        if let fetchedFormattedAddress = self.lookupAddressResults["formatted_address"] as? String{
                            self.fetchedFormattedAddress = fetchedFormattedAddress
                        }
                        
                        if let geometry = self.lookupAddressResults["geometry"] as? Dictionary<String, AnyObject>{
                            let fetchedAddressLongitude = ((geometry["location"] as? Dictionary<String, AnyObject> ?? [:])["lng"] as? NSNumber ?? 0).doubleValue
                            self.fetchedAddressLongitude = fetchedAddressLongitude
                            
                            let fetchedAddressLatitude = ((geometry["location"] as? Dictionary<String, AnyObject> ?? [:])["lat"] as? NSNumber ?? 0).doubleValue
                            self.fetchedAddressLatitude = fetchedAddressLatitude
                        }
                        
                        completionHandler(status, true)
                        
                    }
                    else {
                        completionHandler(status, false)
                    }
                    
                    // use anyObj here
                } catch let error as NSError {
                    print("json error: \(error.localizedDescription)")
                }
            })
        }
        else {
            completionHandler("No valid address.", false)
        }
    }
    

func configureMapAndMarkersForRoute() {
    
    let camera = GMSCameraPosition.camera(withTarget: (Globals.LocationCoordinates?.coordinate)!, zoom: 14)
    
    MapView = GMSMapView.map(withFrame: self.viewNavigation.bounds, camera: camera)
    
    MapView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    originMarker = GMSMarker(position: self.mapTasks.originCoordinate)
    originMarker.map = self.MapView
    originMarker.icon = GMSMarker.markerImage(with: UIColor.green)
    originMarker.title = self.mapTasks.originAddress
    
    destinationMarker = GMSMarker(position: self.mapTasks.destinationCoordinate)
    destinationMarker.map = self.MapView
    destinationMarker.icon = GMSMarker.markerImage(with: UIColor.red)
    destinationMarker.title = self.mapTasks.destinationAddress
    
    self.viewNavigation.addSubview(MapView)

}
    
    func drawRoute() {
        let route = mapTasks.overviewPolyline["points"] as! String
        
        let path: GMSPath = GMSPath(fromEncodedPath: route)!
        routePolyline = GMSPolyline(path: path)
        routePolyline.map = self.MapView
    }
    
     func createRoute() {
        
        let addressAlert = UIAlertController(title: "Create Route", message: "Connect locations with a route:", preferredStyle: UIAlertControllerStyle.alert)
        
        self.mapTasks.getDirections((Globals.LocationCoordinates?.coordinate)!, destinationLocation: (self.destinationCoordinate?.coordinate)!, waypoints: nil, travelMode:MapTask.TravelModes.driving , completionHandler: { (status, success) -> Void in
        
                if success {
                    self.configureMapAndMarkersForRoute()
                    self.drawRoute()
                    self.displayRouteInfo()
                }
                else {
                    print(status)
                }
            })
        }
    
    func displayRouteInfo() {
        
        let lblInfo = UILabel()
        
        lblInfo.text = mapTasks.totalDistance + "\n" + mapTasks.totalDuration
    }
    
}
