//
//  PopUpViewController.swift
//  Locator
//
//  Created by fazal.rahman on 04/06/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit


protocol MenuPopupDelegate: class {
    //func didSelectMenu(selectedMenuID: Int)
}

class PopUpViewController: UIViewController {

    @IBOutlet weak var viewMenu: UIView!
    @IBOutlet weak var viewMyLocations: UIView!
    
    @IBOutlet weak var constViewMenuLeft: NSLayoutConstraint!
    
    @IBOutlet weak var imgMyLoctions: UIImageView!
    @IBOutlet weak var viewMaps: UIView!
    @IBOutlet weak var imgMaps: UIImageView!
    @IBOutlet weak var viewSettings: UIView!
    @IBOutlet weak var imgSettings: UIImageView!
    @IBOutlet weak var viewSignOut: UIView!
    @IBOutlet weak var imgSignOut: UIImageView!
    @IBOutlet weak var imgProfilepic: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
 
    var viewParent: UIViewController?
    
    var menuPopupDelegate:MenuPopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.constViewMenuLeft.constant = -414
        self.view.layoutIfNeeded()
        
        self.imgProfilepic.layer.cornerRadius = self.imgProfilepic.bounds.height / 2
        self.imgProfilepic.clipsToBounds = true
      self.imgProfilepic.image = Globals.profileImage!
        self.lblUsername.text = Globals.loginResponse?["first_name"] as? String
     
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
            //HighlightLastSelectedMenu()
        
            self.constViewMenuLeft.constant = 0.0
        }
    
    func DismissMe(completion: (() -> Swift.Void)?){
        self.constViewMenuLeft.constant = -275
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: {
            (value: Bool) in
            self.dismiss(animated: false, completion: completion)
        })

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func myLocationsClick(_ sender: Any) {
        
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "ChooseLocatorsViewController") as! ChooseLocatorsViewController
        newViewController.viewParent = self
        
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
        
          self.dismiss(animated: false, completion: nil)
    }
//    @IBAction func btnAnywhere(_ sender: Any) {
//
//         DismissMe(completion: nil)
//    }
    @IBAction func btnMenuCloseClick(_ sender: Any) {
        
        
        self.dismiss(animated: false, completion: nil)
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        Globals.mainNavigationController?.popToViewController(viewParent!, animated: true)
        
        
    }
    
    @IBAction func mapsClicks(_ sender: Any) {
        
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        
        newViewController.viewParent = self
        
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
        
        self.dismiss(animated: false, completion: nil)
    }
   
     @IBAction func btnSettingsClick(_ sender: Any) {
        
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        
         newViewController.viewParent = self
        
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
        
         self.dismiss(animated: false, completion: nil)
        
     }
     @IBAction func signOutClick(_ sender: Any) {
        
       
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
        
         self.dismiss(animated: false, completion: nil)
        
        
     }
     
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
