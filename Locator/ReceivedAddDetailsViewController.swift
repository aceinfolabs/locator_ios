//
//  ReceivedAddDetailsViewController.swift
//  Locator
//
//  Created by fazal.rahman on 03/07/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import MapKit

class ReceivedAddDetailsViewController: UIViewController {
    
    @IBOutlet weak var lblCategoryName: UILabel!
    
    @IBOutlet weak var txtdescription: UITextField!
    @IBOutlet weak var imgviewCategory: UIImageView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblLocation: UILabel!
    
    
    @IBOutlet weak var switchfavorite: UISwitch!
    
    var viewParent: UIViewController?

    var categoryname:String?
     var imageData:String?
    
    // save location
    var address:String?
    var country: String?
    var city: String?
    var latitude: CLLocationDegrees?
    var longitude:CLLocationDegrees?
    var name:String?
    var category_id:Int?
    //*
    var saveFindco_ordinates : String?
    var saveCurrentLocationcod : String?
    var saveCurrentMapco_ordinates : CLLocationCoordinate2D?
    var current: String?
    
    var isOn:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        self.btnSave.layer.cornerRadius = 25
        btnSave.clipsToBounds = true
        
        self.dismissKeyboard()
        
       self.lblCategoryName.text = "Choose Category"
        
        switchfavorite.isEnabled = true
        
        // favorite= 0
        //unfavrite = 1
        
        if switchfavorite.isOn == true {
        isOn = 0
        }
        
      //find on map coodinates display on label
        if( saveFindco_ordinates != nil ){
            
           self.lblLocation.text = saveFindco_ordinates
            
        }
        //current location coodinats display on label
        if( saveCurrentLocationcod != nil){
            
            self.lblLocation.text = saveCurrentLocationcod
        }
        
    }
    override func viewWillAppear(_ animated: Bool){
        
        if(categoryname != nil  )
        {
            self.lblCategoryName.text = categoryname as String?
        }
        if (imageData != nil) {
            let url = URL(string:  imageData!)
            if(url != nil){
                Common.GetInstance().getDataFromUrl(url: url!) { data, response, error in
                    guard let data = data, error == nil else { return }
                    
                    DispatchQueue.main.async {
                        self.imgviewCategory.image = UIImage(data: data)
                    }
                }
            }
        }

    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        Globals.mainNavigationController?.popToViewController(viewParent!, animated: true)
   
    }
    
    @IBAction func btnChooseCategoryClick(_ sender: Any) {
        
        Common.GetInstance().ListCategories(completionHandler: getServerResponseCategory)
        
    }
    
   
    
    @IBAction func switchfavoriteClick(_ sender: UISwitch) {

      switchfavorite.isEnabled = false

       if sender.isOn == false {

          isOn = 1
      }
       else{
        
         isOn = 0
        
        }
        
    }
    
    @IBAction func btnSaveClicks(_ sender: Any) {
        
         if(txtdescription.text?.replacingOccurrences(of: " ", with: "") == ""){
            
            Common.GetInstance().showAlert(viewController: self, title: "Add Titite", message: "Please Enter Title.", completion: {(action:UIAlertAction)->Void in
            })
            
            
        }
         else  if(lblCategoryName.text?.replacingOccurrences(of: " ", with: "") == ""){
            
            Common.GetInstance().showAlert(viewController: self, title: "Category", message: "Please Choose a Category.", completion: {(action:UIAlertAction)->Void in
            })
        }
         else{
            
        let user_id = Globals.loginResponse!["id"] as? Int
        
            Common.GetInstance().SaveLocation(address: address!, city:city!, country: country!, latitude: latitude!, longitude: longitude!, name: name!, description: txtdescription.text!, category_id: category_id!, user_id: user_id!,favorite: isOn!, completionHandler: getServerResponseSave)
        }
    }
    func getServerResponseSave(ErrorMessage : String) -> Void {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if(ErrorMessage == "Registration Success"){
                
                let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                
                let issave  = true
                
                newViewController.issave = issave
                Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
                
            }
        }
    }
    
    func getServerResponseCategory(ErrorCode : Int, dataReceived: NSDictionary?, ErrorMessage : String) -> Void {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if(ErrorCode == 0){
                
                let dicLocators = dataReceived
                Globals.categoryDetails = dicLocators!["data"] as? NSArray
                
                let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
                newViewController.newViewControllerReceive = self
                newViewController.categorytitle = self.categoryname
                Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
                
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
