//
//  SearchResultsControllerMap.swift
//  Locator
//
//  Created by fazal.rahman on 12/10/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

protocol LocateOnTheMap{
    func locateWithCoordinate(coordinate:CLLocationCoordinate2D, andTitle title: String)
}

class SearchResultsControllerMap: UITableViewController {
    
    var searchResults: [String]!
    var delegate: LocateOnTheMap!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.searchResults = Array()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadDataWithArray(_ array:[String]){
        self.searchResults = array
        self.tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return self.searchResults.count
        
        }
        
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
            
            cell.textLabel?.text = self.searchResults[indexPath.row]
            return cell
        }
    
    
    
    //address to codinates
        func getCoordinateFrom(address: String, completion: @escaping(_ coordinate: CLLocationCoordinate2D?, _ error: Error?) -> () ) {
    
            CLGeocoder().geocodeAddressString(address) { placemarks, error in
                completion(placemarks?.first?.location?.coordinate, error)
            }
    
        }
    
        override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
    
            self.dismiss(animated: true, completion: nil)
    
            getCoordinateFrom(address: self.searchResults[indexPath.row]) { coordinate, error in
                guard let coordinate = coordinate, error == nil else { return }
    
    
                    print(coordinate)
    
                     self.delegate.locateWithCoordinate(coordinate: coordinate, andTitle: self.searchResults[indexPath.row])
    
            }
            }
}
