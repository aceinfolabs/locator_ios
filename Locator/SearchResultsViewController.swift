//
//  SearchResultsViewController.swift
//  Locator
//
//  Created by fazal.rahman on 27/11/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import MapKit

protocol HandleMapSearch {
    func dropPinZoomIn(placemark:MKPlacemark)
}


class SearchResultsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,MKLocalSearchCompleterDelegate {
    
    
    @IBOutlet weak var tableSearch: UITableView!
   @IBOutlet weak var SearchBar: UISearchBar!
    
    var searchResults = [MKLocalSearchCompletion]()
    var searchCompleter = MKLocalSearchCompleter()
    var selectedPin:MKPlacemark? = nil
    
    var delegate : HandleMapSearch?
   
    var mapView: MKMapView? = nil
    
    var viewParent:UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableSearch.delegate = self
        tableSearch.dataSource = self
        
      
         SearchBar.delegate = self
        searchCompleter.delegate = self
        
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        Globals.mainNavigationController?.popToViewController(viewParent!, animated: true)
        
        navigationController?.popViewController(animated: true)
        
        self.dismiss(animated: false, completion: nil)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = searchResults[indexPath.row]
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = searchResult.title
        //cell.detailTextLabel?.text = searchResult.subtitle
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let completion = searchResults[indexPath.row]
        
        let searchRequest = MKLocalSearchRequest(completion: completion)
        let search = MKLocalSearch(request: searchRequest)
        
        search.start { (response, error) in
            let placemark = response?.mapItems[0].placemark
            
            self.delegate?.dropPinZoomIn(placemark: placemark!)
            Globals.mainNavigationController?.dismiss(animated: true, completion: nil)
            //            print(String(describing: coordinate))
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchCompleter.queryFragment = searchText
    }
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results
        
        tableSearch.reloadData()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
