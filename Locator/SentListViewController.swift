//
//  SentListViewController.swift
//  Locator
//
//  Created by fazal.rahman on 04/12/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class SentListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
   
    @IBOutlet weak var tblSentList: UITableView!
    
   
    @IBOutlet weak var searchBarTable: UISearchBar!
    
    var viewParent: UIViewController?
    var arrMyLocation:NSArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        
        Globals.mainNavigationController?.popToViewController(viewParent!, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
        
      //  return (arrMyLocation?.count)!
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      //  let cellData:NSDictionary = self.arrMyLocation![indexPath.row] as! NSDictionary
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SendTableViewCell", for: indexPath) as! SendTableViewCell


    
   return cell
}
}
