//
//  SettingsViewController.swift
//  Locator
//
//  Created by fazal.rahman on 06/06/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController,MenuPopupDelegate {
    
    var viewParent:UIViewController?

    @IBOutlet weak var viewpProfile: UIView!
    @IBOutlet weak var viewAccount: UIView!
    @IBOutlet weak var viewHelp: UIView!
    @IBOutlet weak var imgProfilePic: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.bounds.height / 2
        self.imgProfilePic.clipsToBounds = true
       
        self.imgProfilePic.image = Globals.profileImage!

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func btnPopUpClick(_ sender: Any) {
        
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: " PopUpViewController") as! PopUpViewController
        
         newViewController.viewParent = self
        
        newViewController.menuPopupDelegate = self
        
        newViewController.modalPresentationStyle = .overCurrentContext
        
         Globals.mainNavigationController?.present(newViewController, animated: false)
    }
    @IBAction func btnProfileClick(_ sender: Any) {
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "UpdateProfileViewController") as! UpdateProfileViewController
        newViewController.viewParent = self
       Globals.mainNavigationController?.pushViewController(newViewController, animated: false)
    }
    
    @IBAction func btnAccountPrivacyClick(_ sender: Any) {
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "UpdateSettingsViewController") as! UpdateSettingsViewController
           newViewController.viewParent = self
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func btnAboutHelp(_ sender: Any) {
        
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: " AboutViewController") as! AboutViewController
         newViewController.viewParent = self
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func btnSettingsClose(_ sender: Any) {
        
    Globals.mainNavigationController?.popViewController(animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
