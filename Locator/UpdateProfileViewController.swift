//
//  UpdateProfileViewController.swift
//  Locator
//
//  Created by fazal.rahman on 07/06/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class UpdateProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var txtMbile: UITextField!
    
    var viewParent: UIViewController?
    
    var arrMyBookings:NSArray?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.hideKeyboardWhenTappedAround()
        
        
       self.btnUpdate.layer.cornerRadius = 25
        btnUpdate.clipsToBounds = true
        
        self.txtMbile.isUserInteractionEnabled = false
        self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.bounds.height / 2
        self.imgProfilePic.clipsToBounds = true
    
//       self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.frame.size.width / 2
//         self.imgProfilePic.clipsToBounds = true
        
        txtUsername.text = Globals.loginResponse?["first_name"] as? String
        txtMbile.text = Globals.loginResponse?["mobile"] as? String
        imgProfilePic.image = Globals.profileImage
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnChangeProfileClick(_ sender: Any) {
     listEditImageButtonClick()
   }

    // to display picked image in imageview
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            imgProfilePic.image = image
            Globals.profileImage = imgProfilePic.image
        }
        else{
            //ERROR MESSAGE
        }
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnUpdateClick(_ sender: Any) {
        
         let user_id = Globals.loginResponse?["id"] as? Int
        Common.GetInstance().UpdateProfile(id: user_id,first_name:txtUsername.text!, completionHandler:getServerResponseUpdateProfile)
    }
   
    func getServerResponseUpdateProfile(Errorcode : Int, dataReceived: Any?, errorMessage : String) -> Void {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.btnUpdate.isEnabled = true
            
            if( Globals.updateStatus == "Update Success"){
                
                Globals.loginResponse?.updateValue(self.txtUsername.text!, forKey: "first_name")
            }
            Common.GetInstance().showAlert(viewController: self, title: "Update Profile", message: "Profile successfully updated.", completion: {(action:UIAlertAction)->Void in
                
                Globals.mainNavigationController?.popViewController(animated: true)

            })
        }
    }
     @IBAction func btnBackClick(_ sender: Any) {
        
       //UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
     Globals.mainNavigationController?.popToViewController(viewParent!, animated: true)
        
     }
    
}
