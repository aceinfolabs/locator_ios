//
//  UpdateSettingsViewController.swift
//  Locator
//
//  Created by fazal.rahman on 08/06/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class UpdateSettingsViewController: UIViewController {
    
      var viewParent: UIViewController?
    
    @IBOutlet weak var btnUpdateSettings: UIButton!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnUpdateSettings.layer.cornerRadius = 25
        btnUpdateSettings.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    @IBAction func btnBackClick(_ sender: Any) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        Globals.mainNavigationController?.popToViewController(viewParent!, animated: true)
    }
    @IBAction func btnUpdateSettingsClick(_ sender: Any) {
        let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
