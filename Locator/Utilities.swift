//
//  Utilities.swift
//  Locator
//
//  Created by fazal.rahman on 20/06/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import Foundation


    extension UIViewController {

        func hideKeyboardWhenTappedAround() {
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
            tap.cancelsTouchesInView = false
            view.addGestureRecognizer(tap)
        }
        
        @objc func dismissKeyboard() {
            view.endEditing(true)
        }
    }

    extension UIViewController {
    
    func listEditImageButtonClick() {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takephoto = UIAlertAction(title: "Take Photo", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            let image = UIImagePickerController()
            image.delegate = (self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate)
            image.sourceType = UIImagePickerControllerSourceType.camera
            image.allowsEditing = false
            self.present(image, animated: true)
            {
            }
            
        })
        
        let choosephoto = UIAlertAction(title: "Choose Photo", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            let image = UIImagePickerController()
            image.delegate = (self as! UIImagePickerControllerDelegate & UINavigationControllerDelegate)
            image.sourceType = UIImagePickerControllerSourceType.photoLibrary //using gallery
            image.allowsEditing = false
            self.present(image, animated: true)
            {
            }
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(takephoto)
        optionMenu.addAction(choosephoto)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
}


