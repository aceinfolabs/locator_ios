//
//  currentMapViewController.swift
//  Locator
//
//  Created by fazal.rahman on 12/09/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class currentMapViewController: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var btnConfirmLocation: UIButton!
    
    var viewParent: UIViewController?
    var iscurrentsent:Bool = true
    
    var code :CLLocationCoordinate2D?
    
    
//     var likelyPlaces: [GMSPlace] = []
//
//    var selectedPlace: GMSPlace?
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    override func viewDidLoad() {
        super.viewDidLoad()
       btnConfirmLocation.layer.cornerRadius = 25
        btnConfirmLocation.clipsToBounds = true
        
       appDelegate.locationManager.requestAlwaysAuthorization()
        
        //map
//        if selectedPlace != nil {
//            let marker = GMSMarker(position: (self.selectedPlace?.coordinate)!)
//            marker.title = selectedPlace?.name
//            marker.snippet = selectedPlace?.formattedAddress
//            // marker.map =  self.Mapview
//        }
//        let marker = GMSMarker()
//
//        marker.position = (Globals.LocationCoordinates?.coordinate)!
        
       // marker.title = (Globals.LocationCoordinates?.coordinate)!

        // marker.map =  self.Mapview
        mapView.showsUserLocation = true
        mapView.delegate = self
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        let annotation = MKPointAnnotation()
        
        annotation.coordinate = Globals.LocationCoordinates!
        
        annotation.title = "current place"
       // annotation.subtitle =
        mapView.addAnnotation(annotation)
        
    }
    
    func geocode(lat: Double, lon: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: lat, longitude: lon)) { placemarks, error in
            guard let placemark = placemarks?.first, error == nil else {
                completion(nil, error)
                return
            }
            completion(placemark, nil)
        }
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        
        Globals.mainNavigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnConfirmLocation(_ sender: Any) {
       
         if let _ = self.viewParent {
            let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "AddDetailsViewController") as! AddDetailsViewController
            
            newViewController.findco_ordinates = Globals.currentlocationco_ordinates
            
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
        }
            
        else{
            let newViewController = Globals.mainStoryboard?.instantiateViewController(withIdentifier: "ReceivedAddDetailsViewController") as! ReceivedAddDetailsViewController
            
            newViewController.saveCurrentLocationcod = Globals.currentlocationco_ordinates
            
            if (Globals.currentlocationco_ordinates != nil) {
            
                geocode(lat: (Globals.LocationCoordinates?.latitude)!, lon: (Globals.LocationCoordinates?.longitude)!) { placemark, error in
                guard let placemark = placemark, error == nil else { return }
                
                let address = "\(placemark.thoroughfare ?? ""), \(placemark.locality ?? ""), \(placemark.subLocality ?? ""), \(placemark.administrativeArea ?? ""), \(placemark.postalCode ?? ""), \(placemark.country ?? "")"
    
                   // print("\(address)")
                    
                    newViewController.address = address
        
                newViewController.country = placemark.country ?? ""
                newViewController.latitude = (placemark.location?.coordinate.longitude)!
                newViewController.longitude  = (placemark.location?.coordinate.latitude)!
                    newViewController.name  = placemark.name ?? ""
                    newViewController.city = placemark.subLocality ?? ""
                }
           
            Globals.mainNavigationController?.pushViewController(newViewController, animated: true)
            }
        }
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

