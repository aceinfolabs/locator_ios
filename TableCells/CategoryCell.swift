//
//  CategoryCell.swift
//  Locator
//
//  Created by fazal.rahman on 16/07/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var lbtCategoryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgCategory.layer.cornerRadius = self.imgCategory.bounds.height / 2
        self.imgCategory.clipsToBounds = true
        
    }
    override func prepareForReuse() {
        //set your cell's state to default here
        self.imgCategory.image = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
