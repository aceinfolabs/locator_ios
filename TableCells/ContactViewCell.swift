//
//  ContactViewCell.swift
//  Locator
//
//  Created by fazal.rahman on 11/07/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class ContactViewCell: UITableViewCell {
    @IBOutlet weak var imgContactImage: UIImageView!
    @IBOutlet weak var lblContactName: UILabel!
    
    @IBOutlet weak var lblContactIndex: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.imgContactImage.layer.cornerRadius = self.imgContactImage.bounds.height / 2
        self.imgContactImage.clipsToBounds = true
        
    }
    
    override func prepareForReuse() {
        self.imgContactImage.image = nil
        
        self.contentView.backgroundColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
