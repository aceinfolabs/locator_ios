//
//  HomeFavoriteCell.swift
//  Locator
//
//  Created by fazal.rahman on 01/06/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class HomeFavoriteCell: UITableViewCell {
    @IBOutlet weak var imglLocation: UIImageView!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblPlace: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imglLocation.layer.cornerRadius = self.imglLocation.bounds.height / 2
        self.imglLocation.clipsToBounds = true
        
        
    }
    
    override func prepareForReuse() {
        //set your cell's state to default here
        self.imglLocation.image = UIImage()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
