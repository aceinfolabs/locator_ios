//
//  LocatorsTableViewCell.swift
//  Locator
//
//  Created by fazal.rahman on 14/08/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class LocatorsTableViewCell: UITableViewCell {

    @IBOutlet weak var imgLocator: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAdress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgLocator.layer.cornerRadius = self.imgLocator.bounds.height / 2
        self.imgLocator.clipsToBounds = true
        
    }

    override func prepareForReuse() {
        //set your cell's state to default here
        self.imgLocator.image = UIImage()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
