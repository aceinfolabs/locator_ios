//
//  SendTableViewCell.swift
//  Locator
//
//  Created by fazal.rahman on 10/12/18.
//  Copyright © 2018 AceInfolabs. All rights reserved.
//

import UIKit

class SendTableViewCell: UITableViewCell {

    @IBOutlet weak var lblAddres: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imgProfileImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgProfileImage.layer.cornerRadius = self.imgProfileImage.bounds.height / 2
        self.imgProfileImage.clipsToBounds = true
        
    }
    override func prepareForReuse() {
        //set your cell's state to default here
        self.imgProfileImage.image = nil
    }
        

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
